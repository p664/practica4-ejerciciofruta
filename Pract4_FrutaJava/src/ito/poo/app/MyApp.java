package ito.poo.app;

import Pract4_FrutaUML.Fruta;
import Pract4_FrutaUML.Periodo;

public class MyApp {

	public static void main(String[] args) {
		
		Fruta fruta1;
		fruta1=new Fruta("Durazno", 92.30f, 537.93f, 1930.60f);
	    System.out.println(fruta1);
	    
	    Periodo periodo;
	    periodo=new Periodo("5 meses", 530.00f);
	    System.out.println(periodo);
	}
}